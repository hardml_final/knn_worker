import os
import pickle

import numpy as np
from flask import Flask, jsonify, request
from sklearn.neighbors import NearestNeighbors

app = Flask(__name__)

DATA_PATH = os.environ['DATA_PATH']

with open(DATA_PATH, 'rb') as f:
    embs: dict = pickle.load(f)
    # если нету данных или они битые, падаем при загрузке, что есть ожидаемое поведение

idx2text: dict = None
knn: NearestNeighbors = None


def train_index(embs: dict):
    global knn, idx2text
    _idx2text = {}
    _embs = []
    for i, (text, emb) in enumerate(embs.items()):
        _idx2text[i] = text
        _embs.append(emb)
    _embs = np.concatenate(_embs)

    _knn = NearestNeighbors(metric='cosine')
    _knn.fit(X=_embs)

    knn = _knn
    idx2text = _idx2text


train_index(embs=embs)
# если в данных какая-то нечитаемая фигня или неподходящий формат, падаем при обучении индексов
del embs

# по хорошему, стоит сюда подгружать уже обученный индекс, а не все данные + обучение,
# а сервинг текстов по индексам выносить в отдельный сервис. Можно даже не хранить их в памяти, а сложить в базу

def get_texts(emb: list, n_neighbors: int):
    emb = np.array([emb])
    idxs = knn.kneighbors(X=emb, n_neighbors=n_neighbors, return_distance=False)[0].tolist()
    texts = [idx2text[idx] for idx in idxs]
    return texts


@app.post('/kneighbors')
def endpoint():
    args = request.args
    emb = request.get_json(force=True)['emb']
    n_neighbors = args.get('n_neighbors', default=5, type=int)
    texts = get_texts(emb=emb, n_neighbors=n_neighbors)
    return jsonify({'texts': texts})


@app.get('/healthcheck')
def healthcheck():
    if not idx2text:
        return jsonify({'status': 'texts itn\'t loaded'}), 504
    if not knn:
        return jsonify({'status': 'knn itn\'t trained'}), 504

    return jsonify({'status': 'ok'})


if __name__ == '__main__':
    app.run(host='0.0.0.0', port=5001)
